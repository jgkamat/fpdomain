# Copyright (C) 2019  Jay Kamat <jaygkamat@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from fpdomain import fpdomain

import pytest


@pytest.mark.parametrize('cached', [True, False])
@pytest.mark.parametrize('host,fp', [
	('jgkamat.gitlab.io', 'jgkamat.gitlab.io'),
	('foo.jgkamat.gitlab.io', 'jgkamat.gitlab.io'),
	('foo.io', 'foo.io'),
	('bbc.co.uk', 'bbc.co.uk'),
	# Wildcards
	('foo.bar.ck', 'bar.ck'),
	# Exceptions
	('foo.www.ck', 'www.ck'),

	# Direct query of a wildcard domain
	('gitlab.io', 'gitlab.io'),
	('com', 'com'),

	# Unknown tlds
	("jg.kamat.qutebrowser", "kamat.qutebrowser"),

	# IP Addresses
	('[::1]', '[::1]'),
	('192.168.1.1', '192.168.1.1'),

	# Stress test
	('a.b.c.d.e.f.g.h.i.com', 'i.com')
])
def test_batch_run(host, fp, cached, benchmark):
	psl = fpdomain.PSL()
	def _test():
		if cached:
			assert psl.fp_domain(host) == fp
		else:
			assert psl.fp_domain.__wrapped__(psl, host) == fp
	benchmark(_test)
